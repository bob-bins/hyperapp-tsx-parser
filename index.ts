import { h, text } from "hyperapp"

export default (nodeName, attributes, ...children) =>
  h(
    nodeName,
    attributes ?? {},
    children
      .flat(1)
      .map(c =>
        (typeof c === "string" || typeof c === "number")
          ? text(c)
          : c
      )
  )
