# hyperapp-tsx-parser

A simple TSX parser for [Hyperapp](https://github.com/jorgebucaran/hyperapp).

## Usage

Install using `npm` or `yarn`

```sh
npm install --dev hyperapp-tsx-parser
```
```sh
yarn add --dev hyperapp-tsx-parser
```

Ensure you have the following configuration in your `tsconfig.json`

```json
{
  "compilerOptions": {
    ...
    "jsx": "react",
    "jsxFactory": "hyperappTsxParser"
  }
}
```

Files containing TSX elements will need the following import statement

```tsx
import hyperappTsxParser from "hyperapp-tsx-parser"
```

### Example

```tsx
import { h, text, app } from "https://unpkg.com/hyperapp"
import hyperappTsxParser from "hyperapp-tsx-parser"

const AddTodo = (state) => ({
  ...state,
  todos: state.todos.concat(state.value),
})

const NewValue = (state, event) => ({
  ...state,
  value: event.target.value,
})

app({
  init: { todos: [], value: "" },
  view: ({ todos, value }) =>
    <main>
      <input type="text" oninput={NewValue}>{value}</input>
      <button onclick={AddTodo}>Add</button>
      <ul>
        {todos.map((todo) => <li>{todo}</li>)}
      </ul>
    </main>,
  node: document.getElementById("app"),
})
```
